import $ from 'jquery';
import validate from "jquery-validation";

//validate form
$.validator.messages.required = 'Заполните поле';

$(function () {
	new Inputmask({mask:'+7 (999) 999-99-99',placeholder: '_'})
		.mask(document.querySelectorAll('.js-phone'));


	const formValidation = function() {
		$('.js-form').each(function () {
			let thisForm = $(this);

			thisForm.validate({
				focusInvalid: false,
				onkeyup: false,
				highlight: function(element) {
					$(element).addClass('input--error');
				},
				unhighlight: function(element) {
					$(element).removeClass('input--error');
				},
				submitHandler: function (form, event) {
					event.preventDefault();

					//EXAMPLE THANK
					thisForm.find('.js-popup-form-content').hide();
					thisForm.find('.popup-form__thank').fadeIn();

					thisForm.find('.btn.submit').addClass('active');
					var data = new FormData(),
						formParams = thisForm.serializeArray();

					$.each(formParams, function (i, val) {
						data.append(val.name, val.value);
					});

					data.append('key', 'value');


					$.ajax({
						url: thisForm[0].action,
						type: 'POST',
						processData: false,
						contentType: false,
						data: data,
						beforeSend: function (data) {
							thisForm.find('button').attr('disabled', 'disabled');
						},
						success: function (data) {
							if (data['error']) {
								console.log('error');
							} else {
								setTimeout(function () {
									thisForm.find('.btn.submit').removeClass('active');

									thisForm.find('.js-popup-form-content').hide();
									thisForm.find('.popup-form__thank').fadeIn();

									thisForm.find('button').prop('disabled', false);

									thisForm[0].reset();
								}, 500);
							}
						},
						error: function (xhr, ajaxOptions, thrownError) {
							console.log(xhr.status);
							console.log(thrownError);
						}
					});
					return false;
				}
			});
		});
	};

	window.formValidation = formValidation;

	formValidation();

	$('.form-group .input').focus(function() {
		let input = $(this).offset().top;
		setTimeout(function () {
			$('.popup').animate({ scrollTop: input }, 600);
		},1500);
	});

});