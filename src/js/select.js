$(function () {
	$B.on('click', '.js-car-select',function () {
		$(this).next('.js-car-list').slideToggle(500);
	});

	$B.on('click', '.js-car-item',function () {
		$('.js-car-list').slideUp();
		const engine = $(this).find('.js-engine').text();
		const type = $(this).find('.js-type').text();
		let val = $(this).data('value');

		$('.js-car-item.active').removeClass('active');

		$(this).addClass('active');

		$(this).closest('.js-parent').find('input').val(val);

		let htmlTemplate = null;
		if (engine && type) {
			htmlTemplate = `
				${engine} <span>${type}</span>
			`;
		} else {
			htmlTemplate = $(this).find('h5').text();
		}

		$('.js-car-select .js-text').html(htmlTemplate);
	});

	$B.bind('touchstart', function(e){
		if ($(e.target).closest('.js-car-list').length || $(e.target).closest('.js-select-list').length) return;
		$('.js-car-list').slideUp();
		$('.js-select-list').fadeOut();
		e.stopPropagation();
	});

	$B.on('click', '.js-select',function () {
		$(this).next('.js-select-list').fadeToggle(500);
	});

	$(document).on('click', '.js-select-item',function () {
		$('.js-select-list').fadeOut();
		const text = $(this).text();
		const val = $(this).data('value');
		const nextElem = $(this).closest('.select-flat').next();
		const info = nextElem.data('info');
		const id = $(this).data('id');
		const allNextElem = $(this).closest('.select-flat').nextAll();

		$('.js-select-item.active').removeClass('active');

		$(this).addClass('active');

		$(this).closest('.select-flat').find('input').attr('value', val).trigger('change');

		const selectedArea = $(this).parent().siblings('.js-select');

		selectedArea.addClass('selected');

		selectedArea.find('h4').text(text);

		if (info !== 'classes' && nextElem.length) {
			const filteredModels = selectData[info].filter(item => item.id == id);
			createList(filteredModels, nextElem.find('.js-select-list'));

		} else if (nextElem.length) {
			const models = $(this).closest('.select-flat').data('info');
			const classesArray = selectData[models].find(item => {
				return item.id == id;
			}).class;
			const classesToShow = [];

			for (let item of selectData[info]) {
				for (let classItem of classesArray) {
					if (item.id == classItem) {
						classesToShow.push(item);
					}
				}
			}

			createList(classesToShow, nextElem.find('.js-select-list'));
		}

		if (info === undefined) {
			const sectionParent = $(this).closest('.popup-form__group');
			sectionParent.find('.js-step').slideUp();
			sectionParent.find('.popup-form__stage svg').fadeIn();
			sectionParent.next().find('.js-step').slideDown();
		}

		if (allNextElem.length) {
			allNextElem.each(function () {
				const elem = $(this).find('.js-select');
				const text = elem.data('placeholder');
				$(this).find('input').attr('value','');
				elem.removeClass('selected');

				$(this).find('.js-select h4').html(text);
			});
		}

	});

	$('.js-select-group').each(function () {
		$(this).on('change paste',function () {
			$(this).removeClass('input--error');
			$(this).next('.error').remove();
		});
	});

	const initMultiSelect = function() {
		$('.js-selct-group').each(function () {
			const element = $(this).find('.select-flat:first-child');
			const slot = element.data('info');
			const list = element.find('.js-select-list');

			createList(selectData[slot], list);
		});
	};

	window.initMultiSelect = initMultiSelect;

	initMultiSelect();

	function createList(array, elem) {
		elem.empty();

		for (let [index,item] of array.entries()) {
			const template = `
			<li class="js-select-item" data-id="${item.id}" data-value="${index}">
				<h4>${item.name}</h4>
			</li>
		`;

			elem.append(template);
		}
	}

});
