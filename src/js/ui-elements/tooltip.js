$(function () {
	$('.js-tooltip').tooltipster({
		theme: ['tooltipster-shadow', 'tooltipster-shadow-customized'],
		maxWidth: 280,
		distance: 5,
		debug: true
	});
});
