$(function () {
	$('.js-list-hide').each(function () {
		let hideCount = $(this).data('show-count');
		let heightAll = 0;
		let heightToShow = 0;

		$(this).find('li').each(function () {
			heightAll += $(this).outerHeight();
		});

		for(let i = 0; i < hideCount; i++) {
			heightToShow += $(this).find('li').eq(i).outerHeight();
		}

		if(!hideCount) return;

		$(this)
			.height(heightToShow)
			.data('heightAll', heightAll)
			.data('heightToShow', heightToShow)
			.data('isOpened', 0);

		const htmlTemplate = `
			<a class="link link--blue expand-list js-expand">Просмотреть все</a>
		`;

		$(this).after(htmlTemplate);
	});

	$('body').on('click', '.js-expand', function (event) {
		event.preventDefault();

		let isOpened = $(this).siblings('.js-list-hide').data('isOpened');
		let heightAll = $(this).siblings('.js-list-hide').data('heightAll');
		let heightToShow = $(this).siblings('.js-list-hide').data('heightToShow');

		$(this).siblings('.js-list-hide').animate({
			height: !isOpened ? heightAll : heightToShow
		},300);

		$(this).siblings('.js-list-hide').toggleClass('active');
		$(this).toggleClass('active');
		$(this).siblings('.js-list-hide').data('isOpened', !isOpened ? 1 : 0);
		$(this).siblings('.js-list-hide').siblings('.js-expand').text(!isOpened ? 'Скрыть' : 'Просмотреть все');
	});

	$('.js-text-expand').each(function () {
		let text = $.trim($(this).text());
		let textLength = text.split(' ').length;

		if (textLength > 58) {
			let textArr = text.split(' ');

			$(this).find('h5').html(textArr.splice(0, 50).join(' ') + '...');

			$(this).attr('data-text',text);

			const htmlTemplate = `
				<a class="link link--blue expand-list js-expand-text">Читать полностью</a>
			`;

			$(this).append(htmlTemplate);
		}
	});

	let isOpened = false;
	$('body').on('click', '.js-expand-text', function () {
		let text = $(this).siblings('h5').text();

		let textToAdd = $(this).parent('.js-text-expand').attr('data-text');

		$(this).parent('.js-text-expand').attr('data-text', text);

		$(this).siblings('h5').html(textToAdd);

		$(this).text(isOpened ? 'Читать полностью' : 'Скрыть');
		isOpened = !isOpened;
	});

});
