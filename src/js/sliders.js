$(function () {

	const baner = new Swiper('.js-main-banner', {
		slidesPerView: 1,
		spaceBetween: 0,
		autoplay: {
			delay: 4000,
		},
		simulateTouch: true,
		spaceBetween: 0,
		effect: 'fade',
		pagination: {
			el: '.swiper-pagination',
			clickable: true

		}
	});

	const info = new Swiper('.js-info-slider', {
		slidesPerView: 1,
		spaceBetween: 9,
		simulateTouch: true,
		spaceBetween: 0,
		breakpoints: {
			1211: {
				slidesPerView: 3,
				simulateTouch: false,
				spaceBetween: 20,
			}
		},
	});


	const best = new Swiper('.js-best-slider', {
		slidesPerView: 1,
		spaceBetween: 9,
		draggable: true,
		watchSlidesVisibility: true,
		breakpoints: {
			534: {
				slidesPerView: 2,
				spaceBetween: 9
			},
			769: {
				slidesPerView: 3,
				spaceBetween: 20
			},
			1024: {
				slidesPerView: 4,
				spaceBetween: 20
			}
		},
	});

	$('.js-arrow-hit .navigation-arrows__item--prev').on('click', function(){
		best.slidePrev();
	});
	$('.js-arrow-hit .navigation-arrows__item--next').on('click', function(){
		best.slideNext();
	});

	const bestTab = new Swiper('.js-best-slider-tab', {
		slidesPerView: 1,
		spaceBetween: 9,
		draggable: true,
		watchSlidesVisibility: true,
		navigation: {
			nextEl: '.car-similar-navigation .navigation-arrows__item--next',
			prevEl: '.car-similar-navigation .navigation-arrows__item--prev',
		},
		breakpoints: {
			534: {
				slidesPerView: 2,
				spaceBetween: 9
			},
			769: {
				slidesPerView: 3,
				spaceBetween: 20,
				draggable: false
			},
			1024: {
				slidesPerView: 4,
				spaceBetween: 20,
				draggable: false
			}
		},
	});


	const action = new Swiper('.js-action-slider', {
		slidesPerView: 1,
		spaceBetween: 9,
		draggable: true,
		watchSlidesVisibility: true,
		breakpoints: {
			534: {
				slidesPerView: 2,
				spaceBetween: 9
			},
			769: {
				slidesPerView: 3,
				spaceBetween: 20,
				draggable: false
			},
			1024: {
				slidesPerView: 4,
				spaceBetween: 20,
				draggable: false
			}
		},
	});

	$('.js-arrow-action .navigation-arrows__item--prev').on('click', function(){
		action.slidePrev();
	});
	$('.js-arrow-action .navigation-arrows__item--next').on('click', function(){
		action.slideNext();
	});

	const services = new Swiper('.js-services-slider', {
		slidesPerView: 1,
		simulateTouch: true,
		spaceBetween: 10,
		breakpoints: {
			1211: {
				slidesPerView: 3,
				simulateTouch: false,
				spaceBetween: 20,
			}
		},
	});

	$('.js-arrow-services .navigation-arrows__item--prev').on('click', function(){
		services.slidePrev();
	});
	$('.js-arrow-services .navigation-arrows__item--next').on('click', function(){
		services.slideNext();
	});

	const feedback = new Swiper('.js-feedback-slider', {
		slidesPerView: 1,
		simulateTouch: true,
		spaceBetween: 0,
		autoHeight: true,
		speed: 700,
		effect: 'fade',
		pagination: {
			el: '.feedback-reviews-pagination',
			clickable: true,
		}, 
		navigation: {
			nextEl: '.feedback-reviews-navigation .navigation-arrows__item--next',
			prevEl: '.feedback-reviews-navigation .navigation-arrows__item--prev',
		}
	});

	const galleryCard = new Swiper('.js-gallery-card', {
		slidesPerView: 1,
		simulateTouch: true,
		spaceBetween: 0,
		autoHeight: true,
		speed: 700,
		pagination: {
			el: '.swiper-pagination',
			clickable: true,
		}, 
		navigation: {
			nextEl: '.used-car__navigation .navigation-arrows__item--next',
			prevEl: '.used-car__navigation .navigation-arrows__item--prev',
		}
	});


	const gallerySlideSettings = {
		slidesPerView: 1,
		autoHeight: true,
		spaceBetween: 0,
		navigation: {
			nextEl: '.card-slider-navigation .navigation-arrows__item--next',
			prevEl: '.card-slider-navigation .navigation-arrows__item--prev',
		},
		pagination: {
			el: '.swiper-pagination',
			type: 'bullets',
			clickable: true
		}
	};

	const galleryFilter = new Swiper('.js-gallery-filter', gallerySlideSettings);
	const gallery = new Swiper('.js-gallery', gallerySlideSettings);

	window.galleryFilter = galleryFilter;
	window.gallery = gallery;

	const usedCar = function () {
		new Swiper('.js-used-car', {
			slidesPerView: 1,
			spaceBetween: 9,
			autoHeight: true,
			pagination: {
				el: '.swiper-pagination',
				type: 'bullets',
				clickable: true,
			}
		});
	};

	usedCar();

	window.usedCar = usedCar;

	// expansion tabs slider
	const tabsOption = {
		slidesPerView: 1,
		autoHeight: true,
		effect: 'fade',
		touchRatio: 0,
	};

	const tabs = new Swiper('.js-tab-slide', tabsOption);

	window.tabs = tabs;

	tabsController($('.js-tab'),tabs,false);

	// car card bottom slider
	const tabsFilter = new Swiper('.js-filter-slide',tabsOption);

	tabsController($('.js-tab-filter'),tabsFilter,false);

	function tabsController($elem,slider,isForm) {
		$elem.on('click',function () {
			let index = $(this).data('index');

			$elem.each(function () {
				if ($(this).hasClass('active')) $(this).removeClass('active');
			});

			$(this).addClass('active');

			slider.slideTo(index, 300);

			if (isForm) {
				$(this).parent().find('input').val(index);
				$('.js-car-baner').css('background',$(this).data('color'));
			}
		});
	}

	//car color filter
	const carColor = new Swiper('.card-baner-img',{
		slidesPerView: 1,
		autoHeight: true,
		effect: 'fade',
		on: {
			slideChange: function () {
				const elem = $('.card-color__item.js-color');
				$('.card-color__item.js-color.active').removeClass('active');
				$('#carcolor').val(this.activeIndex);
				elem.eq(this.activeIndex).addClass('active');
				$('.js-car-baner').css('background',elem.eq(this.activeIndex).data('color'));
			}
		}
	});

	tabsController($('.js-color'), carColor, true);


	const eqp = new Swiper('.js-eqp-slider', {
		slidesPerView: 1,
		spaceBetween: 9,
		draggable: true,
		autoHeight: false,
		watchSlidesVisibility: true,
		breakpoints: {
			700: {
				slidesPerView: 2,
				spaceBetween: 20,
				autoHeight: false
			},
			1210: {
				slidesPerView: 4,
				spaceBetween: 20,
				autoHeight: false
			}
		},
	});

	$('.js-eqp-navigation .navigation-arrows__item--prev').on('click', function(){
		eqp.slidePrev();
	});
	$('.js-eqp-navigation .navigation-arrows__item--next').on('click', function(){
		eqp.slideNext();
	});


});



