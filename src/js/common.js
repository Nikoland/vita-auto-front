$(function () {
	function setPaginatinActiveClass() {
		let activeIndex = $('.pagination .active').index();

		if (activeIndex) $(`.pagination a:nth-child(${activeIndex})`).addClass('prev')
	}

	setPaginatinActiveClass();

	$(".car-buttons").each(function () {
		let offset = $(this).offset().top;
		$(window).on('scroll',function () {
			if (offset < $W.scrollTop()) {
				$('.header').addClass('js-header-btn');
				$('.js-path').fadeOut();
				$('.js-btn').slideDown();
			} else {
				$('.header').removeClass('js-header-btn');
				$('.js-path').fadeIn();
				$('.js-btn').slideUp();
			}
		});
	});


	$('.js-more-block').on('click',function () {
		$('.js-more-hidden').toggleClass('active');


		const isOpened = $(this).data('isopened');

		$(this).html(isOpened ? 'Показать еще' : 'Скрыть' );

		$(this).data('isopened', !isOpened);
	});

});
