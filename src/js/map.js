$(function () {
	if ($('#map').hasClass('contacts__map')) {
		ymaps.ready(function () {
			const $map = $('#map');
			let lat = $map.data('lat');
			let lng = $map.data('lng');

			let myMap = new ymaps.Map('map', {
				center: [parseFloat(lat),parseFloat(lng)],
				zoom: 14
			}, {
				searchControlProvider: 'yandex#search'
			});

			let myPlacemark = new ymaps.Placemark(myMap.getCenter(), {}, {
				iconLayout: 'default#image',
				iconImageHref: __app.TEMPLATE_URI + 'images/marker.png',
				iconImageSize: [30, 38],
				iconImageOffset: [-15, -19]
			});

			myMap.geoObjects.add(myPlacemark);
		});
	}
});
