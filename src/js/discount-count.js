$(function () {
	let total = 0;
	const checkbox = $('.js-checkbox');

	checkbox.on('change', function () {
		const price = $(this).data('price');
		if ($(this).find('input').prop("checked")) {
			total += price
		} else {
			total -= price;
		}
		$('.js-total').html(total + ' ₽');
	});

	checkbox.each(function () {
		if ($(this).find('input').prop("checked")) {
			const price = $(this).data('price');
			total += price
		}
	});

	$('.js-total').html(total + ' ₽');
});
