$(function () {
	const $popup = $('.js-popup');
	const $popupCloseBtn = $('.js-popup-close');
	let lastOpenedPopup = null;
	let scrollPosition;

	if (location.hash !== '') {
		openPopup(location.hash.substring(1), location.href);
	}

	$('body').on('click','.js-popup-link',function(e){
		e.preventDefault();
		let $this = $(this);
		let target = $this.attr('data-target');
		let href = $this.attr('data-href');

		openPopup(target, href);

		return false;
	});

	function openPopup(target, href) {
		const $elem = $(`#${target}`);
		$popup.removeClass('is-show');
		$elem.addClass('is-show');

		scrollPosition = $(window).scrollTop();

		if (href) {
      history.pushState({}, '', href);
      location.hash = target;
			$elem.find('.js-wrapper').empty();
			getEventItem(href,$elem);
			lastOpenedPopup = 'href';
		}
	}

	window.openPopup = openPopup;

	$popupCloseBtn.on('click',function(){
		let $this = $(this);
		let $popup = $this.closest('.js-popup');

		$popup.removeClass('is-show');
		$popup.find('.js-wrapper').empty();
		$('.js-popup-form-content').show();
		$('.popup-form__thank').fadeOut();
		location.hash = '';
		if (scrollPosition !== "undefined") {
			$(window).scrollTop(scrollPosition);
			scrollPosition = undefined;
		}

		if (lastOpenedPopup == 'href') {
			history.go(-3);
			location.hash = '';
			lastOpenedPopup = null;
		}

	});

	function getEventItem(item,elem) {
		$.ajax({
			url: item,
			type: "GET",
			success: function(data) {
				if ($(data).length !== 0) {
					elem.find('.js-wrapper').html($(data).hide().fadeIn(1000));
					initMultiSelect();
					formValidation();
					new Inputmask({mask:'+7 (999) 999-99-99',placeholder: '_'})
						.mask(document.querySelectorAll('.js-phone'));
				}
			}
		});
	}

});

