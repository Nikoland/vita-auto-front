$(function () {
	$('.js-header-menu').on('click',function () {
		let dropDown = $(this).data('id');

		$(dropDown).slideToggle(500);
		$(this).find('svg').toggle();
        $('.js-header-sub').fadeToggle();
	});

	$('.js-close-menu').on('click',function () {
		$('.js-menu').slideUp(500);
        $('.js-header-sub').fadeOut();
	});



//fixed summenu in card

	let $capsuleMenu = $('.js-card-nav'),
        $headerH = $('.header').outerHeight();

    function capsuleMenu(){
        let $offsetMenu = $('.card-wrapper').offset().top - $headerH
        if ($W.scrollTop() > $offsetMenu) {
            $capsuleMenu.addClass('fixed');
        } else {
            $capsuleMenu.removeClass('fixed');
        }
    }

    if ($capsuleMenu.length) {
        capsuleMenu();
        $W.scroll(function(){
            capsuleMenu();
        });
    }

//scroll to 
	let $scrollto = $('.js-scrollto');
	let $header = $('.header');
	let H = $('.js-card-nav').height() + $header.height();

    $scrollto.on('click', function(e) {
        e.preventDefault();

        $scrollto.removeClass('active');
        $(this).addClass('active');

        let elementClick = $(this).attr("href");
        let destination = $(elementClick).offset().top - H;

        $("html:not(:animated),body:not(:animated)").animate({
                scrollTop: destination
        }, 300);
    });

    $W.scroll(function(){
        $scrollto.removeClass('active');

    });

});
