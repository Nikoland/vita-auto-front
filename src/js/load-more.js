let $masonryGrid = $('.js-grid');

$W.on('load', function(){

    $masonryGrid.masonry({
        itemSelector: '.js-grid__item',
        columnWidth: '.js-grid__sizer',
        percentPosition: true,
        horizontalOrder: true,
        isAnimated: true,
        gutter: '.gutter-sizer'
    });
});


$(function () {
		const $loadingButton = $('.js-more');
    let counter = 0;

    $loadingButton.on("click", function(e) {
        e.preventDefault();
        let href = $(this).attr('href');

        $.ajax({
            url: href,
            type: "GET",
            data: {
                page: counter += 1
            },
            success: function(data){
                if ($(data).length !== 0) {
                    $('.js-more-wrapper').append($(data).hide().fadeIn(1000));
                    $masonryGrid.masonry( 'reloadItems' );
                    $masonryGrid.masonry( 'layout' );
					usedCar();
                } else {
                    $loadingButton.remove();
                }
            }
        });

    });
});
