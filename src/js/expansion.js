$(function(){
	$('.js-expansion-head').on('click', function () {
		$(this).next('.js-expansion-body').slideToggle(500);
		$(this).toggleClass('active');

		const galleryArrays = [gallery,galleryFilter,tabs];

		for (let item of galleryArrays) {
			refreshGallery(item);
		}

		function refreshGallery(slider) {
			if (Array.isArray(slider)) {
				for (let item of slider) {
					item.update();
				}
			} else {
				slider.update();
			}
		}

	});
});
