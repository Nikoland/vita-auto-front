import 'babel-polyfill';
import svg4everybody from 'svg4everybody';
import $ from 'jquery';
import Inputmask from "inputmask";
import Swiper from 'swiper';
import 'tooltipster/dist/js/tooltipster.main.min';
import 'tooltipster/dist/js/tooltipster.bundle.min';
import jQueryBridget from 'jquery-bridget';
import Masonry from 'masonry-layout';
jQueryBridget( 'masonry', Masonry, $ );


window.$ = $;
window.jQuery = $;
window.Swiper = Swiper;
window.Inputmask = Inputmask;
window.$W = $(window);
window.$D = $(document);
window.$H = $('html');
window.$B = $('body');




svg4everybody();
import 'ninelines-ua-parser';
