$(function () {
	$D.on('focus','.js-input',function(){
		$(this).parent('.form-field').addClass('form-field--focused');
		$(this).parent('.form-field').removeClass('form-field--error');
	});

	$D.on('blur','.js-input',function(){
		if ($(this).val() === ''){
			$(this).parent('.form-field').removeClass('form-field--focused');
		}
	});

	// Set focused class if have value
	function setInputFocused() {
		$('.js-input').each(function () {
			if ($(this).val() === ''){
				$(this).parent('.form-field').removeClass('form-field--focused');
			} else {
				$(this).parent('.form-field').addClass('form-field--focused');
			}
		});
	}

	setInputFocused();
});
